import React, { Component } from 'react';
import ViewForm from './ViewForm/ViewForm';
import { Button, Modal, ModalBody } from 'reactstrap';
import './style.css';

class ViewContainer extends Component {
    constructor(){
        super();
        this.state = {
            headers: [],
            rows: [],
            photoIndices: [],
            showModal: false,
            selectedImage: '',
            modalRotate: "90",
            width: 100,
            exceptions: []
        }
    } 

    componentDidMount(){
        this.formatTable(this.props.form);
        
    }

    formatTable = async (form) => {
        let headers = [];
        let rows = [];
        await form.map((row , i) => {
            if(i === 0){
                headers = row;
                this.formatHeaders(headers);
            }else{
                rows.push(row);
            }
        });

        this.formatRows(rows);
    }

    formatHeaders = async (headers) => {
        let photoIndices = [];
        let exceptions = [];
        const formatted = headers.map((header, i) => {
            let tag;
            const match = header.match(/<(.*?)>/g);
            const exceptionHeaders = [
                'form_record_id',
                'Inventory Number:<text>',
                'Grave information<section>',
                'Marker information (measurements in feet and inches):<section>',
                'Inscriptions<section>',
                'Ornamentation / Decoration<section>',
                'Overall setting<section>',
                'Marker description<section>',
                'Plot accessories<section>',
                'Additional notes<section>'
            ]
            if(!exceptionHeaders.includes(header)){
                if(match){
                                tag = match[0];
                                if(tag === "<photo>"){
                                    photoIndices.push(i);
                                }
                            }
                            return (
                                <th className="table-th" key={i} >{header}</th>
                            )
            }else{
                console.log(`pushing ${header}`);
                exceptions.push(i);
                
            }


            // '  form_record_id ,   Inventory Number:  , ,   Marker information (measurements in feet and inches):<section> ,  inscriptions<section>   Ornamentation / Decoration<section>  ,  Overall setting<section>  , Marker description<section> , Plot accessories<section> , Additional notes<section>'
            
        });

        await this.setState({
            headers: formatted,
            photoIndices,
            exceptions
        });
    }

    formatRows = (rows) => {
        console.log("THE EXCEPTIONS: ", this.state.exceptions);
        const formattedRows = rows.map((row, i) => {
            const formattedRow = row.map((field, i) => {
                let finalField = field;
                if(this.state.photoIndices.includes(i)){
                    if(field){
                        const fieldArr = field.split("; " );
                        if(fieldArr.length < 2){
                            finalField = <img key={i} alt="your" onClick={this.handleImageClick} className="table-thumbnail" name={field} src={require('../assets/data/HGM-RGulch/Historic Grave Marker Inventory & Assessment Record Test/Historic Grave Marker Inventory & Assessment Record-Photos/' + field)} />
    
                        }else{
                            finalField = fieldArr.map((img, i) => {
                                return(
                                    <img key={i} alt="your" onClick={this.handleImageClick} className="table-thumbnail" name={img} src={require('../assets/data/HGM-RGulch/Historic Grave Marker Inventory & Assessment Record Test/Historic Grave Marker Inventory & Assessment Record-Photos/' + img)} />
                                )
                            })
                        }
                    }
                }
                if(!this.state.exceptions.includes(i)){
                    return(
                        <td key={i} >{finalField}</td>
                    )   
                }
                
            });
            return(
                <tr key={i}>
                    {formattedRow}
                </tr>
            )
        });

        this.setState({
            rows: formattedRows
        });

        // this.props.toggleLoading();
    }

    handleImageClick = (e) => {
        this.setState({
            selectedImage: e.target.name
        });
        this.toggleModal();
    }

    handleRotate = (e) => {
        const newRotate = parseInt(this.state.modalRotate) + parseInt(e.target.name);
        this.setState({
            modalRotate: newRotate
        });
    }

    toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        });
    }

    handleWheel = (e) => {
        console.log(e.deltaY)
        const inc = e.deltaY * -1;
        const changer = inc/10;
        const newWidth = this.state.width + changer;
        this.setState({
            width: newWidth
        });
    }

    render(){
        return(
            <div>
                {this.props.showForm ? <ViewForm toggleLoading={this.props.toggleLoading} headers={this.state.headers} rows={this.state.rows} /> : null }

                <Modal isOpen={this.state.showModal} toggle={this.toggleModal} className="table-modal">
                    <div className="modal-buttons">
                        <img src={require('../assets/images/rotate-counterclockwise-90.png')} name="-90" className="modal-rotate-left" onClick={this.handleRotate} />
                        <Button className="modal-cancel" color="secondary" onClick={this.toggleModal}>X</Button>
                        <img src={require('../assets/images/rotate-clockwise-90.png')} name="90" className="modal-rotate-right" onClick={this.handleRotate} />
                    </div>

                    <ModalBody>
                        { this.state.selectedImage ? <img alt="your" onWheel={this.handleWheel} style={{transform: `rotate(${this.state.modalRotate}deg)`, width: this.state.width + "%"}} src={require('../assets/data/HGM-RGulch/Historic Grave Marker Inventory & Assessment Record Test/Historic Grave Marker Inventory & Assessment Record-Photos/' + this.state.selectedImage)} /> : null }
                    </ModalBody>

                </Modal>

            </div>
        )
    }
}

export default ViewContainer;