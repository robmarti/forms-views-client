import React, { Component } from 'react';
import { Table } from 'reactstrap';
import './style.css';

class ViewForm extends Component {
    componentDidMount(){
        this.props.toggleLoading();
    }
    render(){
        return(
            <div>
                <div>
                    <Table dark >
                        <thead >
                            <tr>
                                {this.props.headers}
                            </tr>
                        </thead>
                        <tbody className="form-body">
                            {this.props.rows}
                        </tbody>
                    </Table>
                </div>
            </div>
        ) 
    }
    
};

export default ViewForm;