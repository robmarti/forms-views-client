import React, { Component } from 'react';
import { Button } from 'reactstrap';
import './style.css';

class Header extends Component {
    constructor(){
        super();

    }

    handleClick = (e) => {
        this.props.toggleLoading();
        this.props.getCSV(e.target.name);

    }

    render(){
        return(
            <div className="arch-div">
                <h1>View Cody's CSV</h1><br/><br/>
                <p>Cody's CSV:</p><br/><br/>
                <Button onClick={this.handleClick} name={this.props.csvPath}>View Historic Grave Marker Inventory & Assessment Record</Button>
                <img src={require("../assets/images/arch-gif-01.gif")} className="arch-gif" />
            </div>
        )
    }
}

export default Header;