import React, { Component } from 'react';
import './App.css';
import ViewContainer from './ViewContainer/ViewContainer';
import Header from './Header/Header';

class App extends Component {
  constructor(){
    super();
    this.state = {
      csvPath: "Historic Grave Marker Inventory & Assessment Record Test",
      showForm: false,
      isLoading: false,
      form: [],
    }
  }

  getCSV = async (path) => {
    try{
      console.log(path);
        const response = await fetch('http://localhost:3001/upload/' + path);
        const parsedResponse = await response.json();

        this.setState({
          form: parsedResponse.data,
          showForm: true,
        });

    }catch(err){
        console.error(err);
    }
  }

  toggleLoading = () => {
    console.log('toggling')
    this.setState({
      isLoading: !this.state.isLoading
    })
  }

  render(){
    console.log(this.state.isLoading);
    return (
      <div className="App">
        { this.state.showForm ? <ViewContainer 
                                  showForm={this.state.showForm} 
                                  form={this.state.form} 
                                  toggleLoading={this.toggleLoading}/> : this.state.isLoading ?
                                  <img src={require('./assets/images/arch-gif-02.gif')} className="loading-gif" /> :
                               <Header getCSV={this.getCSV} csvPath={this.state.csvPath} toggleLoading={this.toggleLoading} /> }
      </div>
    )
  }
  
}

export default App;
